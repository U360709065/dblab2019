select * from Customers;
delete from Customers where CustomerId = 20;

select * from orders where customerId = 20;
delete from orders where customerId = 20;

select * from orderdetails where orderId in (
	select orderId from orders where customerId = 20
);

delete from orderdetails where orderId in (
	select orderId from orders where customerId = 20
);

delete from orders where customerId = 20;

delete from Customers where CustomerId = 20;

select * from products;
delete from products where productId = 10;

select * from orderdetails where productid = 10;
delete from orderdetails where productid= 10;
delete from products where productId = 10;

select * from shippers;
delete from shippers where shipperid = 2;

select * from orders where shipperid=2;
delete from orders where shipperid = 2;

select * from orderdetails where orderid in (
	select orderid from orders where shipperid=2
);

delete from orderdetails where orderid in (
	select orderid from orders where shipperid=2
);

delete from orders where shipperid = 2;
delete from shippers where shipperid = 2;